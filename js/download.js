var overlay = document.querySelector(".overlay")
var blackArch = document.querySelector("#black-arch")
var gaming = document.querySelector("#gaming")
var popup = document.querySelectorAll(".popup")

var overflow = false;
function showPopup(ed) {
    if (ed === "gaming") {
        console.log(ed)
        gaming.classList.toggle("show-popup")
    }
    else if (ed === 'blackarch') {
        console.log(ed)
        blackArch.classList.toggle("show-popup")
    }
    else {
        popup.forEach(p => {
            p.classList.remove("show-popup")
        })
    }
    overlay.classList.toggle("active");
    overflow = overflow ? false : true;
    hideOverflowWhenOverlayIsActive()
}
overlay.addEventListener('click', () => {
    showPopup("")
}
)

function hideOverflowWhenOverlayIsActive() {
    if (overflow) {
        document.querySelector("body").style.overflow = 'hidden';
    }
    else {
        document.querySelector("body").style.overflow = 'visible';
    }
}